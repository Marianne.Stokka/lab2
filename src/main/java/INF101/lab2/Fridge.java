package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {
    // List of items in fridge
    ArrayList<FridgeItem> itemList = new ArrayList<FridgeItem>(20);
    int maxSize = 20;

    @Override
    public int totalSize() {
        return maxSize;
    }

    @Override
    public void emptyFridge() {
        itemList.clear();
        
    }

    @Override
    public int nItemsInFridge() {
        return itemList.size();
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (nItemsInFridge() < 20) {
            itemList.add(item);
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<FridgeItem>();
        for (FridgeItem item : itemList) {
            if (item.hasExpired()) {
                expiredFood.add(item);
            }
        }
        itemList.removeAll(expiredFood);
        return expiredFood;
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!itemList.contains(item)) {
            throw new NoSuchElementException();
        }
        else {
            itemList.remove(item);
        }
    }
    
}
